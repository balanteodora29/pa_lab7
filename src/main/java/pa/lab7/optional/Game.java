package pa.lab7.optional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game {
    public final Board board;
    List<Player> players;
    boolean gameEnded = false;
    public int winningScore;

    public Game(Board board, int winningScore, Player... players) {
        this.board = board;
        this.players = Arrays.asList(players);
        for (Player player : players) {
            player.setGame(this);
        }
        this.winningScore = winningScore;
    }

    public void setGameEnded(boolean gameEnded) {
        this.gameEnded = gameEnded;
    }

    public boolean isGameEnded() {
        return gameEnded;
    }

    public void start() {
        List<Thread> threads = new ArrayList<Thread>();

        for (Player player : players) {
            Thread thread = new Thread(player);
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for(Thread thread: threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
