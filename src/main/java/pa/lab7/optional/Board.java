package pa.lab7.optional;

import java.util.ArrayList;
import java.util.List;

public class Board {
    int n, m;
    List<Token> tokens = new ArrayList<Token>();

    public Board(int n) {
        this.n = n;
        this.m = n;
        for (int i = 1; i <= n; i++) {
            Token token = new Token();
            tokens.add(token);
        }
    }

    public int getNumberOfAvailableTokens() {
        int result = 0;
        for (Token token : tokens)
            if (!token.isAvailable())
                result++;
        return result;
    }
}
