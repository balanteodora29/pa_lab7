# Laboratorul 7

[Link cerinta](https://profs.info.uaic.ro/~acf/java/labs/lab_07.html)

## Compulsory
Create the following components:

1. Create classes in order to model the problem. You may assume that all possible tokens are initially available, having random values.
2. Each player will have a name, and they must perform in a concurrent manner, extracting repeatedly tokens from the board.
3. Simulate the game using a thread for each player.  
   Pay attention to the synchronization of the threads when extracting tokens from the board.


## Rezolvare Compulsory

1. I created the classes Board, Token, Game, Player and Main;
   * Board has the dimensions of the board (n, m) and a list of tokens;
   * Token has a pair of numbers(between 0 and 9), a value(between 0 and 50) and a boolean variable available(is true when the token is on the board);
   * Game has a board, a list of players, a boolean value telling as if the game ended, and a winning score(first player who gets this score, wins);  
     The method start creates a thread for each player in the game;
   * Player has a name, a game, a number of points, and a sequence(a list of tokens);  
     The class Player implements the interface Runnable. I overrode the method run to simulate the game;
   * In the Main class I created 3 players, a board and a game.
     
     
## Optional

1. Implement the scoring and determine who the winner is at the end of the game.  
2. Make sure that players wait their turns, using a wait-notify approach.  
3. Consider the situation when each player might have a different strategy for extracting a number: automated (random) or manual.  
4. A manual player will use the keyboard, while the bot will extract a random token. Simulate bot contests on large graphs.  
5. Implement a timekeeper thread that runs concurrently with the player threads, as a daemon. This thread will display the running time of the game and it will stop the game if it exceeds a certain time limit.

## Rezolvare Optional

1. A Game object has a winningScore parameter - when the score of a player is equal or greater then that, the games stops and on the screen apears "The winner is X";

