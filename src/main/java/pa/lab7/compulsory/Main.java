package pa.lab7.compulsory;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Teo");
        Player player2 = new Player("Timo");
        Player player3 = new Player("Floki");

        Board board = new Board(100);
        Game game = new Game(board, 200, player1, player2, player3);
        game.start();
    }
}
