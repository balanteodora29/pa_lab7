package pa.lab7.compulsory;

import java.util.Arrays;

public class Token {
    private int[] pair = {0, 0};
    private int value;
    private boolean available;

    public Token(int[] pair, int value){
        this.pair = pair;
        this.value = value;
        this.available = false;
    }

    public Token(){
        pair[0] = (int) (Math.random() * 10);
        int rand = (int) (Math.random() * 10);
        while (pair[0] == rand)
            rand = (int) (Math.random() * 10);
        pair[1] = rand;
        value = (int) (Math.random() * 50);
        this.available = false;
    }

    public int[] getPair() {
        return pair;
    }

    public void setPair(int[] pair) {
        this.pair = pair;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "{" +
                "pair=" + Arrays.toString(pair) +
                ", value=" + value +
                '}';
    }
}

