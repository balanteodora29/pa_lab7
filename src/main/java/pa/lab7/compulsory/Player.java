package pa.lab7.compulsory;

import java.util.ArrayList;
import java.util.List;

public class Player implements Runnable {
    private String name;
    private Game game;
    private int points;
    private List<Token> sequence = new ArrayList<>();

    public Player(String name){
        this.name = name;
    }

    /**
     * The run method simulates the game in the following method:
     * - While we still have tokens on the board and the game hasn't ended,
     *    - We use a synchronized block to allow only one thread(a player in our case) to execute at a time,
     *       - We go through all the tokens and choose the one with the first number of the pair equals to the last number of the last token in our sequence,
     *       - If our sequence is empty, we took the first available token;
     *       - If one player has a number of points equal or greater than the winning score, we stop the while loop and print to the screen the winner;
     *    - The sleep method allows the players to take turns.
     */
    @Override
    public void run() {
        while (this.game.board.getNumberOfAvailableTokens() != 0 && !game.gameEnded){
            synchronized (game.board){
                boolean madeMove = false;
                for (Token token : game.board.tokens){
                    if (!madeMove){
                        if (!token.isAvailable()){
                            if (sequence.size() != 0) {
                                if (token.getPair()[0] != sequence.get(sequence.size() - 1).getPair()[1]) {
                                    continue;
                                }
                            }
                            sequence.add(token);
                            points += token.getValue();
                            token.setAvailable(true);
                            System.out.println(name + ": I took " + token);
                            madeMove = true;
                            if (this.points >= game.winningScore){
                                game.gameEnded = true;
                                System.out.println("The winner is " + this.name + " with a score of " + this.points);
                                break;
                            }
                        }
                    }
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Game ended!!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
